Blockly.Msg.CPEKU_MQTT_CONNECT = "เชื่อมต่อกับโบรกเกอร์ MQTT";
Blockly.Msg.CPEKU_MQTT_PORT = "พอร์ท";
Blockly.Msg.CPEKU_MQTT_CONNECT_TOOLTIP = "เชื่อมต่อกับโบรกเกอร์ MQTT";

Blockly.Msg.CPEKU_MQTT_PUBLISH = "ประกาศข่าวในหัวข้อ";
Blockly.Msg.CPEKU_MQTT_WITH_MSG = "ด้วยข้อมูล";
Blockly.Msg.CPEKU_MQTT_PUBLISH_TOOLTIP = "ประกาศข่าวด้วยหัวข้อและข้อมูลที่ระบุ";

Blockly.Msg.CPEKU_MQTT_ON_RECV = "เมื่อได้รับข่าวในหัวข้อ";
Blockly.Msg.CPEKU_MQTT_ON_RECV_TOOLTIP = "ขอรับข่าวสารตามหัวข้อที่ระบุและกำหนดชุดคำสั่งเพื่อดำเนินการเมื่อได้รับข่าว";

Blockly.Msg.CPEKU_MQTT_CONNECTED = "เชื่อมต่อกับโบรกเกอร์ได้";
Blockly.Msg.CPEKU_MQTT_CONNECTED_TOOLTIP = "ตรวจสอบสถานะว่ามีการเชื่อมต่อกับโบรกเกอร์อยู่หรือไม่";

Blockly.Msg.CPEKU_MQTT_MSG_TEXT = "ข้อความที่ได้รับล่าสุด";
Blockly.Msg.CPEKU_MQTT_MSG_TEXT_TOOLTIP = "คืนค่าข้อมูลที่ได้รับล่าสุดในรูปข้อความ";

Blockly.Msg.CPEKU_MQTT_MSG_NUMBER = "ค่าตัวเลขที่ได้รับล่าสุด";
Blockly.Msg.CPEKU_MQTT_MSG_NUMBER_TOOLTIP = "คืนค่าข้อมูลที่ได้รับล่าสุดในรูปค่าตัวเลข";
