#include <functional>
#include <map>

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <esp_log.h>
#include <tcpip_adapter.h>

#include "kidbright32.h"
#include "mqtt_client.h"
#include "cpeku_mqtt.h"

static const char *TAG = "CPEKU_MQTT";

esp_mqtt_client_handle_t CPEKU_MQTT::_client;
CPEKU_MQTT::state_t CPEKU_MQTT::_state;
CPEKU_MQTT::CallbackMap CPEKU_MQTT::_cbmap;
char CPEKU_MQTT::_received_text[64];

CPEKU_MQTT::CPEKU_MQTT() {
  _client = nullptr;
  _received_text[0] = 0;
}

void CPEKU_MQTT::init(void) {
  _state = DETECT;
  esp_log_level_set(TAG,LOG_LOCAL_LEVEL);
}

void CPEKU_MQTT::process(Driver *drv) {
  switch (_state) {
    case DETECT:
      // clear error flag
      error = false;
      // set initialized flag
      initialized = true;
      break;
    case CONNECTED:
      break;
    case DISCONNECTED:
      break;
  }
}

void CPEKU_MQTT::connect(const char* host, uint16_t port) {
  // make sure an IP address is obtained
  ESP_LOGI(TAG, "awaiting IP address");
  tcpip_adapter_ip_info_t ipInfo; 
  while (1) {
    tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);
    if (ipInfo.ip.addr != 0)
      break;
    vTaskDelay(100 / portTICK_RATE_MS);
  }
  ESP_LOGI(TAG, "IP address detected");

  // initialize and start MQTT client
  ESP_LOGI(TAG, "connecting to MQTT broker '%s' port %d",host,port);
  esp_mqtt_client_config_t mqtt_cfg;
  memset(&mqtt_cfg,0,sizeof(mqtt_cfg));
  mqtt_cfg.event_handle = _event_handler;
  mqtt_cfg.host = host;
  mqtt_cfg.port = port;
  _client = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_start(_client);

  // make sure MQTT is connected before returning
  while (!connected())
    vTaskDelay(100 / portTICK_RATE_MS);

  // connected; turn on IoT LED
  gpio_set_level(IOT_LED_GPIO, LED_ON);
  ESP_LOGI(TAG, "MQTT client connected");
}

void CPEKU_MQTT::publish(const char* topic, const char* msg) {
  if (!connected()) return;
  esp_mqtt_client_publish(_client, topic, msg, 0, 0, 0);
  ESP_LOGI(TAG, "publish string '%s' to %s", msg, topic);
}

void CPEKU_MQTT::publish(const char* topic, double value) {
  if (!connected()) return;
	char buf[64];
	sprintf(buf, "%g", value);
  esp_mqtt_client_publish(_client, topic, buf, 0, 0, 0);
  ESP_LOGI(TAG, "publish value %g to %s", value, topic);
}

void CPEKU_MQTT::on_message(const char* topic, OnMessageCallback cb) {
  _cbmap[topic] = cb;
  if (connected()) {
    esp_mqtt_client_subscribe(_client, topic, 0);
  }
}

char* CPEKU_MQTT::received_text() {
  return _received_text;
}

double CPEKU_MQTT::received_number() {
  return strtod(_received_text,nullptr);
}

void CPEKU_MQTT::_handle_data(char* topic, int topic_len, char* data, int data_len) {
  char topic_buf[64];

  if (topic_len > sizeof(topic_buf)-1) {
    topic_len = sizeof(topic_buf)-1;
  }
  strncpy(topic_buf,topic,topic_len);
  topic_buf[topic_len] = 0;

  if (data_len > sizeof(_received_text)-1) {
    data_len = sizeof(_received_text)-1;
  }
  strncpy(_received_text,data,data_len);
  _received_text[data_len] = 0;

  // find the matched topic and pass the value to the corresponding callback
  auto it = _cbmap.find(topic_buf);
  if (it != _cbmap.end()) {
    ESP_LOGI(TAG, "callback for topic \"%s\" found",topic_buf);
    it->second();
  }
  else {
    ESP_LOGI(TAG, "callback for topic \"%s\" not found",topic_buf);
  }
}

esp_err_t CPEKU_MQTT::_event_handler(esp_mqtt_event_handle_t event)
{
  esp_mqtt_client_handle_t client = event->client;
  switch (event->event_id) {
    case MQTT_EVENT_CONNECTED:
      _state = CONNECTED;
      ESP_LOGI(TAG, "broker connected");
      for (auto& kv : _cbmap) {
        ESP_LOGI(TAG, "subscribe to '%s'",kv.first.c_str());
        esp_mqtt_client_subscribe(_client, kv.first.c_str(), 0);
      }
      break;
    case MQTT_EVENT_DISCONNECTED:
      _state = DISCONNECTED;
      ESP_LOGI(TAG, "broker disconnected");
      gpio_set_level(IOT_LED_GPIO, LED_OFF);
      break;
    case MQTT_EVENT_SUBSCRIBED:
      break;
    case MQTT_EVENT_UNSUBSCRIBED:
      break;
    case MQTT_EVENT_PUBLISHED:
      break;
    case MQTT_EVENT_DATA:
      _handle_data(event->topic, event->topic_len, event->data, event->data_len);
      break;
    case MQTT_EVENT_ERROR:
      break;
    default:
      break;
  }
  return ESP_OK;
}
